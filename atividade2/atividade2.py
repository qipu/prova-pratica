import sys, os, json
import scrapy
from scrapy.crawler import CrawlerProcess

# base: https://www.digitalocean.com/community/tutorials/como-fazer-crawling-em-uma-pagina-web-com-scrapy-e-python-3-pt

class MySpider(scrapy.Spider):
    name = 'qipu_spider'
    start_urls = []
    scrapy_reponse = []

    def parse(self, response):
        del self.scrapy_reponse[:]
        SET_SELECTOR = 'div.col-lg-4.order-sm-12'
        for brickset in response.css(SET_SELECTOR):

            SUNRISE_SELECTOR = 'sunrise ::text'
            SUNSET_SELECTOR = 'sunset ::text'
            METAR_AND_TAF_SELECOR = 'h5.mb-0.heading-primary + p ::text'
            CARTAS_SELECTOR = 'h4 + ul li a ::text'
            CARTAS_LINKS_SELECTOR = 'h4 + ul li a ::attr(href)'

            cartas_text = brickset.css(CARTAS_SELECTOR).getall()
            cartas_links = brickset.css(CARTAS_LINKS_SELECTOR).getall()

            cartas = []
            for c_index in range(len(cartas_text)):
                cartas.append({
                    'name': cartas_text[c_index],
                    'link' : cartas_links[c_index]
                })

            temp1 = {
                'sunrise': brickset.css(SUNRISE_SELECTOR).extract_first(),
                'sunset': brickset.css(SUNSET_SELECTOR).extract_first(),
                'meatar': brickset.css(METAR_AND_TAF_SELECOR).getall()[0].replace('\n', ''),
                'taf': brickset.css(METAR_AND_TAF_SELECOR).getall()[1].replace('\n', ''),
                'cartas' : cartas,
            }

            self.scrapy_reponse.append(temp1)
            yield temp1



def main(code):
    process = CrawlerProcess({
        'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'
    })

    MySpider.start_urls = ['https://www.aisweb.aer.mil.br/?i=aerodromos&codigo='+code]
    process.crawl(MySpider)
    process.start() # the script will block here until the crawling is finished

    os.system('clear') # clear console


    # print(MySpider.scrapy_reponse)
    print(json.dumps(MySpider.scrapy_reponse, indent=4, sort_keys=False)) # pretty json


if __name__ == "__main__":

    code = sys.argv[1]
    main(code)


else:
    print("This module can not be imported")