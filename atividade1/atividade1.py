#!/urb/bin/env python
# _*_ coding: utf-8 _*_

# font: https://www.hardware.com.br/comunidade/algoritmo-escrever/1224976/

import sys,re


def _inFill(num):

    units = ["zero", "um", "dois", "três", "quatro",
                "cinco", "seis", "sete", "oito", "nove"]

    teens = ["dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezessete", "dezoito", "dezenove"]

    tens = ["dez", "vinte", "trinta", "quarenta", "cinquenta",
            "sessenta", "setenta", "oitenta", "noventa"]

    # convert to string
    number=str(num)

    # Fill the string with zeros until it is 3 characters long:
    number.zfill(3)


    a=int(number[0]) # hundreds
    b=int(number[1]) # tens
    c=int(number[2]) # units

    # 0xy
    if a == 0:
        # 00x
        if b == 0:
            result=units[c]
            return result

        # 01x
        elif b == 1:
            if c >= 0 and c <= 9:
                result = teens[c]
                return result

        # 02x
        elif b == 2:
            if c == 0:
                result = 'vinte'
                return result
            elif c > 0 and c <= 9:
                result ='vinte e '+units[c]
                return result

        # 0[3-9]x
        elif b >=3 and b <= 9:
            if c == 0:
                result = tens[b-1]
                return result
            if c >= 1 and c <= 9:
                result = tens[b-1]+' e '+units[c]
                return result
    # 1xy
    if a == 1:
        if b == 0:
            if c == 0:
                result = 'cem'
                return result
            elif c > 0 and c <= 9:
                result ='cento e '+units[c]
                return result
        elif  b == 1:
            if c >= 0 and c <= 9:
                result = 'cento e '+teens[c]
                return result
        elif b == 2:
            if c == 0:
                result = 'cento e vinte'
                return result
            elif c > 0 and c <= 9:
                result ='cento e vinte e '+units[c]
                return result
        elif b >= 3 and b <= 9:
            if c == 0:
                result = 'cento e '+tens[b-1]
                return result
            elif c > 0 and c <= 9:
                result = 'cento e '+tens[b-1]+ ' e '+units[c]
                return result

    # [2-9]xy
    elif a >= 2 and a <= 9:
        # 200
        if a == 2 and b ==0 and c == 0:
            prefix='duzentos'
        # 2xy
        elif a ==2:
            prefix='duzentos e '

        # 300
        if a == 3 and b ==0 and c == 0:
            prefix='trezentos'
        # 3xy
        elif a == 3:
            prefix='trezentos e '

        # 400
        if a == 4 and b ==0 and c == 0:
            prefix='quatrocentos'
        # 4xy
        elif a == 4:
            prefix='quatrocentos e '

        # 500
        if a == 5 and b ==0 and c == 0:
            prefix='quinhentos'
        # 5xy
        elif a == 5:
            prefix='quinhentos e '

        # 600
        if a == 6 and b ==0 and c == 0:
            prefix='seiscentos'
        # 6xy
        elif a == 6:
            prefix='seiscentos e '

        # 700
        if a == 7 and b ==0 and c == 0:
            prefix='setecentos'
        # 7xy
        elif a == 7:
            prefix='setecentos e '

        # ...
        if a == 8 and b ==0 and c == 0:
            prefix='oitocentos'
        elif a == 8:
            prefix='oitocentos e '

        # ...
        if a == 9 and b ==0 and c == 0:
            prefix='novecentos'
        elif a == 9:
            prefix='novecentos e '

        # [2-9]0x
        if b == 0:
            # [2-9]00
            if c == 0:
                result = prefix
                return result
            # [2-9]0x
            elif c > 0 and c <= 9:
                result = prefix+units[c]
                return result

        # [2-9]1x
        elif b == 1:
            # [2-9]1x
            if c >= 0 and c <= 9:
                result = prefix+teens[c]
                return result
            #elif c >= 6 and c <= 9:
                #resultado = prefix+tens[b-1]+' e '+unidades[c]
                return result

        # [2-9]2x
        elif b == 2:
            # [2-9]20
            if c == 0:
                result = prefix+'vinte'
                return result
            # [2-9]2x
            elif c > 0 and c <= 9:
                result = prefix+'vinte e '+units[c]
                return result
        # ...
        elif b >= 3 and b <= 9:
            # ...
            if c == 0:
                result = prefix+tens[b-1]
                return result
            # ...
            elif c > 0 and c <= 9:
                result = prefix+tens[b-1]+' e '+units[c]
                return result

def numberToInFill(num):
    '''
    Convert a number to in fill format

    Input: number
    output: String

    eg:
    1,00 => um real
    1000,54 => mil reais e cinquenta e quatro centavos
    '''

    result=''
    number=str(num)
    # number=number.zfill(9)+number
    number=number.zfill(9)

    # check if is 'mill' 'milhoes' or ''
    # get groups of 3 characters
    for i in [0,3,6]:
        var=number[i]+number[i+1]+number[i+2]
        if int(var) != 0:
            res=_inFill(var)
            if i == 0:
                result=res+" milhões "
            elif i == 3:
                result=result+res+" mil "
            elif i == 6:
                result=result+res
    return result

def checkRules(number):
    '''
    Rules of number:

    - Must be number
    - before `,` : 9 digits
    - after `,` : 2 digits

    input: number
    output: boolean
    '''

    number = number.replace(",", ".")

    number_splitted = re.split(r'[,.]', number)

    if(len(number_splitted) == 1):
        return number_splitted[0].isdigit() and len(number_splitted[0]) <= 9
    elif (len(number_splitted) == 2):
        return number_splitted[0].isdigit() and len(number_splitted[0]) <= 9 and number_splitted[1].isdigit() and len(number_splitted[1]) <= 2
    else:
        return False


if __name__ == "__main__":
    number = sys.argv[1]
    # print('testing: '+str(number))
    # print(numberToInFill(number))

    # valid input
    if (not checkRules(number)):
        print('Entrada invalida\n'+
            'Regra:\n'+
            'O programa deve aceitar valores sempre com dois digitos após a virgula e com até 9 dígitos antes da vírgula.')
        sys.exit()

    number = number.replace(",", ".")

    number_splitted = re.split(r'[,.]', number)

    if(len(number_splitted) == 1):
        print(numberToInFill(number_splitted[0])+' reais')
    elif (len(number_splitted) == 2):
        print(numberToInFill(number_splitted[0])+' reais e '+ numberToInFill(number_splitted[1])+' centavos')
